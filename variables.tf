# Input variable definitions

variable "ecr_name" {
  description = "Name of ECR"
  type        = string
  default     = "ecr-repository"
}
