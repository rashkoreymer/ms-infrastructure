module "ecr" {
  source  = "hazelops/ecr/aws"
  version = "1.0.2"
  name    = var.ecr_name
}
