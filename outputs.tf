# Output variable definitions

output "ecr_id" {
  description = "ECRs id"
  value       = module.ecr.repository_id
}

output "ecr_url" {
  description = "ECRs URL"
  value       = module.ecr.repository_url
}
